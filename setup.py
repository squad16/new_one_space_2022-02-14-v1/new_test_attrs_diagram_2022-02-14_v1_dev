from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='new_test_attrs_diagram_2022-02-14_v1_dev',
    packages=find_packages(where='src', exclude=['tests']),
    package_dir={'': 'src'},
    version=version,
    description='new_test_attrs_diagram',
    author='po_1 po_1'
)
